var gulp       = require('gulp')
  , gutil      = require('gulp-util')
  , sass       = require('gulp-sass')
  , jade       = require('gulp-jade')
  , sourcemaps = require('gulp-sourcemaps')
  , minify     = require('gulp-cssnano')
  , js_concat  = require('gulp-concat')
  , js_uglify  = require('gulp-uglify')
  , rename     = require('gulp-rename')
  , prefix     = require('gulp-autoprefixer');



// # SASS
gulp.task('sass', function() {
  return gulp.src('public/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', onError)
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'));
});

// # JADE
gulp.task('jade', function() {
  return gulp.src('views/index_dev.jade')
      .pipe(jade())
      .pipe(gulp.dest('./'));
});

// # WATCH
gulp.task('watch', function() {
  gulp.watch('public/sass/**/*.scss', ['sass']);
  gulp.watch('views/**/*.jade', ['jade']);
});





// # BUILD
gulp.task('build', ['jade-build', 'js-build', 'sass-build']);

gulp.task('js-build', function(){
  return gulp.src(['public/js/vendor/wow.min.js', 'public/js/vendor/disable.scroll.min.js', 'public/js/vendor/scrollspy.js', 'public/js/main.js'])
    .pipe(js_concat('concat.js'))
    .pipe(rename('main.min.js'))
    .pipe(js_uglify())
    .pipe(gulp.dest('public/js'))
});

gulp.task('jade-build', function() {
  return gulp.src('views/index.jade')
      .pipe(jade())
      .pipe(gulp.dest('./'));
});

gulp.task('sass-build', function() {
  return gulp.src('public/sass/**/*.scss')
    .pipe(sass())
    .on('error', onError)
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(minify())
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('public/css'));
});




function onError(err) {
  console.log(err);
  this.emit('end');
}