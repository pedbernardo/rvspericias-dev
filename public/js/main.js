// # MODAL
var modal = {
  init: function() {
    // # triggers
    $(document).bind("keyup", modal.escKey );
    $(".js-service-more").click( modal.open );
    $(".js-modal-close").click( modal.close );
    $(".js-btn-budget").click( modal.budgetOpen );
    $(".js-btn-fast-budget").click( modal.fastBudget );
    $(".modal").click( modal.watch );
  },
  open: function( e ) {
    e.preventDefault();

    var pos  = modal.getPos(),
        href = $(this).attr("href")

    if ( href != undefined ) {
      var idText = "#" + href
        , $card  = $(".services-grid").find("a[href='"+ href +"']").closest(".card")
        , title  = $card.find(".title-text").text()
        , icon   = $card.find(".title-icon").attr('class').split(' ').pop();

      modal.showText(idText, title, icon);
    }
    else {
      $("#modal-services").find(".title-icon").addClass("icon--");
    }

    $("body").addClass("no-scroll");
    $(".modal-wrapper").css("marginTop", pos + "px");
    $(".modal").addClass("active");
    $(".modal-panel").addClass("animated bounceInDown");
  },
  showText: function( id, title, iconClass ) {
    var $modal = $("#modal-services");

    $(id).show();
    $modal.find(".title-text").text(title);
    $modal.find(".title-icon").addClass(iconClass);
  },
  escKey: function( e ) {
    if ( e.which == 27 ) modal.close();
  },
  watch: function( e ) {
    if ( e.target.className === "modal-wrapper" ) modal.close();
  },
  close: function() {
    $(".modal").removeClass("active");
    $(".modal-panel").removeClass("animated bounceInDown");
    $("body").removeClass("no-scroll");

    modal.reset();
  },
  getPos: function() {
    var pos = $("body").scrollTop();
    return pos;
  },
  budgetOpen: function() {
    $(".js-modal-budget").removeClass("modal--disable")
                         .addClass("animated bounceInRight");

    $(".js-modal-services").addClass("modal--disable");
  },
  fastBudget: function( e ) {
    e.preventDefault();

    $(".js-modal-services").addClass("modal--disable");
    modal.open( e );
    modal.budgetOpen();
  },
  reset: function() {
    $(".js-modal-budget").removeClass("animated bounceInRight")
    .addClass("modal--disable");

    setTimeout(function() {
      var $modal    = $("#modal-services")
        , iconClass = $modal.find(".title-icon").attr('class').split(' ').pop();

      $(".js-modal-services").removeClass("modal--disable");
      $(".modal-text").hide();
      $modal.find(".title-text").text("");
      $modal.find(".title-icon").removeClass(iconClass);
    }, 400);
  }
};


// # TOGGLE MOBILE NAV
var nav = {
  init: function() {
    $(".js-nav-open").click( nav.open );
    $(".js-nav-close").click( nav.close );
    $(".nav-item").click( nav.close );
  },
  open: function() {
    $(".main-nav").addClass("nav--open");
  },
  close: function() {
    $(".main-nav").removeClass("nav--open");
  }
};


// # SEND BY AJAX CONTACT FORM
var contactForm = {
  init: function() {
    $(".js-send-contact").click( contactForm.submit );
  },
  submit: function() {
    var $form    = $('.contact-form')
      , validate = $form[0].checkValidity();

    if (validate) {

      var $self     = $(".js-send-contact")
        , $step     = $self.find(".btn-step:eq(1)")
        , stepText  = $step.text()
        , loadIcon  = "<i class='icon--refresh a-icon--spin'></i>";

      $step.text("").append(loadIcon);

      var formData = contactForm.getData();
      var send = $.ajax({
        url: "//formspree.io/contato@rvspericias.com.br",
        method: "POST",
        data: formData,
        dataType: "json"
      });
      send.done(function() {
        $self.addClass("is-success").delay(1500).queue(function(){
          $step.text(stepText);
          $self.removeClass("is-success").dequeue();
          $(".contact-form")[0].reset();
        });
      });
      send.fail(function() {
        $self.addClass("is-error").delay(1500).queue(function(){
          $step.text(stepText);
          $self.removeClass("is-error").dequeue();
        });
      });
    }
    else {
      $(".js-contact-submit").click();
    }
  },
  getData: function() {
    var $form = $('.contact-form');
    var data  = {};
    $form.serializeArray().map(function(x){data[x.name] = x.value;});

    return data;
  }
};

// # SEND BY AJAX CONTACT FORM
var budgetForm = {
  init: function() {
    $(".js-send-budget").click( budgetForm.submit );
  },
  submit: function() {
    var $form    = $('.bugdget-form')
      , validate = $form[0].checkValidity();

    if (validate) {

      var $self     = $(".js-send-budget")
        , $step     = $self.find(".btn-step:eq(1)")
        , stepText  = $step.text()
        , loadIcon  = "<i class='icon--refresh a-icon--spin'></i>";

      $step.text("").append(loadIcon);

      var formData = budgetForm.getData();
      var send = $.ajax({
        url: "//formspree.io/orcamento@rvspericias.com.br",
        method: "POST",
        data: formData,
        dataType: "json"
      });
      send.done(function() {
        $self.addClass("is-success").delay(1500).queue(function(){
          $step.text(stepText);
          $self.removeClass("is-success").dequeue();
          $(".bugdget-form")[0].reset();
        });
      });
      send.fail(function() {
        $self.addClass("is-error").delay(1500).queue(function(){
          $step.text(stepText);
          $self.removeClass("is-error").dequeue();
        });
      });
    }
    else {
      $(".js-budget-submit").click();
    }
  },
  getData: function() {
    var $form = $('.bugdget-form');
    var data  = {};
    $form.serializeArray().map(function(x){data[x.name] = x.value;});

    console.log(data);
    return data;
  }
};

$( function() {
  // # init core functions
  modal.init();
  nav.init();
  contactForm.init();
  budgetForm.init();
  // ----------------


  // # init scrollspy
  $("#nav").scrollspy({ animate: true });
  // ----------------

  // # init WOW animations
  wow = new WOW({
                  boxClass:     'a-scroll',
                  animateClass: 'animated',
                  offset:       0,
                  mobile:       true,
                  live:         true
                })
  wow.init();
  // ----------------


  // # ease scroll on href=# links
  var $root = $('html, body');

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

      if (target.length) {
        $root.animate({
          scrollTop: target.offset().top
        }, 800);
        return false;
      }
    }
  });
  // ----------------


  // # fix nav when below hero section
  var $win = $(window)
  , fixedNav = function() {
    ( $win.scrollTop() > $(".main-content").offset().top -1 ) ? $(".main-nav, .header-mobile").addClass("nav--fixed") : $(".main-nav, .header-mobile").removeClass("nav--fixed") ;
  };
  $win.scroll( fixedNav );
  // ----------------
});


